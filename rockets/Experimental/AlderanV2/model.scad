/*
	This Rocket should exists to show how to fork your 
	own design.
*/


use<../../modules/NoseCones/Parabolic_NEG1/model.scad>
use<../../modules/Tanks/LOX_NEG1/model.scad>
use<../../modules/Engines/Aerospike_NEG1/model.scad>
use<../../modules/Fins/FIN_NEG1/model.scad>
use<../../modules/Tanks/SEARS_NEG1/model.scad>


module AlderanV2(){

	color([0.4,0.4,0.4]) {
		rotate([180,0,0])scale([19.3,19.3,20]) translate([0,0,-020]) SEARS_NEG1();
	}

	color([0.6,0.2,0.2]){ 
		translate([0,0,-35]) Aerospike_NEG1();
	}
}

AlderanV2();

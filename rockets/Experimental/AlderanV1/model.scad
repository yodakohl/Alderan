use<../../modules/NoseCones/Parabolic_NEG1/model.scad>
use<../../modules/Tanks/LOX_NEG1/model.scad>
use<../../modules/Engines/Aerospike_NEG1/model.scad>
use<../../modules/Fins/FIN_NEG1/model.scad>


module AlderanV1(){

	color([0.4,0.4,0.4]) {
		translate([0,0,-1500]) Parabolic_NEG1();
		translate([0,0,-1000]) LOX_NEG1();
		translate([0,0,-500]) LOX_NEG1();
		translate([0,0,0]) LOX_NEG1();
	}

	color([0.6,0.2,0.2]){ 
		translate([0,0,-35]) Aerospike_NEG1();
		translate([0,0,0]) FIN_NEG1();
	}
}

AlderanV1();
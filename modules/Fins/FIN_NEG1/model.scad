module fin()
{

linear_extrude(height=1, convexity=10, slices=20, scale = 1.0) {
	polygon(points= [[20,-16],[-10,-16], [-80,-20],[-6,-40]]);
}
};

module FIN_NEG1()
{

	scale([5,5,5]){
	rotate([0,90,0]) union(){
	rotate([0,0,0]) fin();
	rotate([120,0,0]) fin();
	rotate([240,0,0]) fin();
	}
	}

}

//cylinder(1,100,100);

FIN_NEG1();
include <Paraboloid.scad>

module Parabolic_NEG1()
{
	difference(){
		translate([0,0,-240]) paraboloid (y=250,f=10,rfa= 0,fc=1,detail=50);
		translate([0,0,-230]) paraboloid (y=250,f=10,rfa= 0,fc=1,detail=50);
	}
	cylinder(1,100,100);
}

Parabolic_NEG1();

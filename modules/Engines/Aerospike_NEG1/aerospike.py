import math

def func(index):

	res = 1*math.exp((-0.5)*math.pow(index,2))
	#e^(-index^2);
	return res

def write_coord(index,file):
	f.write("[")
	f.write(str(func(index)))
	f.write (",")
	f.write (str((index)))
	f.write("]")


print "Creating Aerospike"


f = open('aerospike.scad', 'w')

f.write("module aerospike_nozzle()\n{\n")
f.write("rotate_extrude($fn=200)  polygon(")
f.write("points=[")


stop = 2.5
start = -2.5
step = 0.10

i = start
while i < stop:
	write_coord(i,f)
	if i != stop:
		f.write(",")
	i = i+step

write_coord(start,f)
f.write("]") 
f.write(");\n");
f.write("}"); 
#f.write("aerospike_nozzle();")
f.close()



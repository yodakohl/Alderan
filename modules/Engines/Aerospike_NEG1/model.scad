use <aerospike.scad>
use <end.scad>
module Aerospike_NEG1()
{

	translate([0,0,100]) scale([5,5,5]){
	
	//Chamber
	union(){
	difference() {
		 translate([0,0,5])cylinder(h = 16, r = 10, center = true);
		 cylinder(h = 50, r = 8, center = true);
	}

	//translate([0,0,-4]) cylinder(h = 2, r = 10, center = true);
	}
		
	
	//Nozzle
	difference(){
	union(){
	translate([0,0,18]) scale([5,5,5]) aerospike_nozzle();
	translate([0,0,6]) cylinder(h = 18, r = 3, center = true);
	translate([0,0,20]) cylinder(h = 18, r = 0.5, center = true);
	}
		translate([0,0,33]) cylinder(h = 10, r = 4, center = true);
	}
	
	//End Cap
	difference(){
		translate([0,0,16]) cylinder(h = 6, r = 10, r2 =7 ,center = true);
		translate([0,0,14]) cylinder(h = 12, r = 10.1, r2 =5 ,center = true);
	}

	translate([0,0,-4]) cylinder(h = 2, r = 10, r2 =10 ,center = true);

	
	end();
	}
}


Aerospike_NEG1();

module end()
{

	difference() {
		translate([0,0,0])cylinder(h = 26, r = 20, r2 = 10, center = true);
		union(){
			cylinder(h = 30, r = 18, r2 = 9, center = true);
			cylinder(h = 200, r = 10, r2 = 10, center = true);
		}
	}

}

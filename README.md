Alderan
=======

An open source rocket project aiming to create fully 3d printable, small scale and 
modular rockets. See /doc for more information.
Rockets are like distributions, modules like packages. 

All new rockets are created in the experimenal folder.
Rockets which are tested and verified are moved from Experimental 
to Stable folder. 

###How to contribute:
* Create a new module
* Create a new rocket 
* Run CFD simulations
* Various programming in python
* Concepts and Documentation

All generated Files can be viewed at https://www.dropbox.com/sh/3gbyl0s17h2vlut/RbLO7Y0s6z/Alderan

# Desings a nozzle and prints elementary data.
# USE ONlY SI UNITS
# Input is
#	- a given fuel
#	- desired thrust
#	- design altitude
#	- chamber pressure


import math

#Constants (Settings ) ##########################################

#pAmbient = 0.002549	#Ambient Pressure		[MPa]
pAmbient = 0.010325	#Sea Level Pressure		[MPa]
#pAmbient = 0.00001	#Near Vacuum			[MPa]


#pChamber = 100		#Chamber Pressure		[MPa]
pChamber = 1.068	#Chamber Pressure		[MPa]

k = 1.4			#Specific Heat Ratio
Runi = 8.3144621	#Gas Constant 			[J/mol*K]
#molarMass = 0.01801	#Molar Mass H2O 		[kg/mol]
#molarMass = 0.00201	#Molar Mass Hydrogen rich	[kg/mol]
molarMass = 0.006	#Molar Mass Hydrogen rich	[kg/mol]


T1 = 2740		#Combustion Chamber Temperature [K]
fThrust = 200		#Desired Thrust in		[N]

#################################################################

print "Calculating Laval Nozzle"
print "--------------------------------------------------------"
print "Design Ambient Pressure: " + str(pAmbient) 	+" [MPa]"
print "Chamber Pressure: " + str(pChamber) 		+" [MPa]"
print "Desired Thrust: " + str(fThrust) 		+" [N]"
print "Chamber Temperature: " + str(T1) 		+" [K]"
print "--------------------------------------------------------"
print "--------------------------------------------------------"



R = Runi/molarMass
pressureRatio = pAmbient/pChamber
pCritical = pChamber * ((2/(k+1))**(k/(k-1)))
vThroat = math.sqrt(((2*k)/(k+1))*R*T1)
vExit = math.sqrt(( ((2*k)/(k-1))*R*T1)*(1 - (pressureRatio**((k-1)/k))))
massFlow = fThrust / vExit
V1 = (R*T1)/(pChamber*1e6)
Vt = V1*(math.pow(((k+1)/2),(1/(k-1))))
V2 = V1*(math.pow((pChamber/pAmbient),(1/k)))
A1 = (massFlow*V1)
At = (massFlow*Vt)/vThroat
A2 = (massFlow*V2)/vExit
dt = math.sqrt(At/3.1415)*200
d2 = math.sqrt(A2/3.1415)*200
expansionRatio = A2/At
ISP = fThrust / (massFlow*9.81)

print "Critical Pressure: " + 	str(pCritical)		+" [MPa]"
print "Throat Velocity: " + 	str(vThroat)		+" [m/s]"
print "Exit Velocity: " + 	str(vExit)		+" [m/s]"
print "Pressure Ratio: " + 	str(pressureRatio)
print "Mass Flow: " + 		str(massFlow)		+" [kg/s]"
print "Volume Input: " + 	str(V1)			+" [m3/kg]"
print "Volume Throat: " + 	str(Vt)			+" [m3/kg]"
print "Volume Exit: " + 	str(V2)			+" [m3/kg]"
print "Area Chamber: " + 	str(A1)			+" [m2]"
print "Area Throat: " + 	str(At)			+" [m2]"
print "Area Exit: " + 		str(A2)			+" [m2]"
print "Diameter Throat: " +	str(dt)			+" [cm]"
print "Diameter Exit: " +	str(d2)			+" [cm]"
print "Expansion Ratio: " +	str(expansionRatio)
print "ISP: " 		+	str(ISP)		+" [s]"








